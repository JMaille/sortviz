import { IState } from "../interfaces/AppInterfaces";
import { Colors, colorNode } from "../utils/Colors";

export function bubbleSortIterate(
  state: IState,
  callback: (newState: IState) => void
) {
  let { data, list, sortedLimit, sortNode, compareNode, step } = {
    ...state,
  };

  if (sortedLimit && sortedLimit.prev !== list.begin()) {
    switch (step) {
      case 1:
        if (compareNode) colorNode(list.find(compareNode.data), Colors.default);
        if (sortNode) {
          colorNode(list.find(sortNode.data), Colors.sorting);
          compareNode = sortNode.next;
          if (compareNode)
            colorNode(list.find(compareNode.data), Colors.compare);
        }

        step = 2;
        break;
      case 2: // If sortNode > compareNode, swap them
        if (
          sortNode &&
          compareNode &&
          sortNode.data.val > compareNode.data.val
        ) {
          const newNodes = list.swap(sortNode, compareNode);
          sortNode = newNodes.newNode1;
          compareNode = newNodes.newNode2;
          data = list.traverse();
          if (sortNode.next && sortNode.next !== sortedLimit) step = 1;
          else step = 4;
        } else {
          if (
            sortNode &&
            sortNode.next &&
            compareNode &&
            compareNode.next !== sortedLimit
          ) {
            step = 3;
          } else {
            step = 4;
          }
        }
        break;
      case 3:
        if (compareNode) colorNode(list.find(compareNode.data), Colors.default);
        if (sortNode) {
          colorNode(list.find(sortNode.data), Colors.default);
          sortNode = sortNode.next;
        }
        if (sortNode) {
          colorNode(list.find(sortNode.data), Colors.sorting);
          compareNode = sortNode.next;
          if (compareNode)
            colorNode(list.find(compareNode.data), Colors.compare);
        }

        step = 2;
        break;
      case 4:
        if (compareNode && compareNode.next === sortedLimit) {
          if (sortNode) colorNode(list.find(sortNode.data), Colors.default);
          sortNode = compareNode;
          compareNode = null;
        }
        if (sortNode) colorNode(list.find(sortNode.data), Colors.sorted);

        sortedLimit = sortNode;
        sortNode = list.begin();
        step = 1;
        break;
      default:
        console.error("Unknown step : " + step + ". Possibly dead-locked.");
        break;
    }
  } else {
    if (sortNode) colorNode(list.find(sortNode.data), Colors.sorted);
    sortedLimit = null;
    compareNode = null;
    step = 0;
  }

  callback({ ...state, data, list, sortedLimit, sortNode, compareNode, step });
}
