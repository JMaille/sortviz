import { Point } from "../utils/CustomTypes";
import { Node } from "../utils/LinkedList";
import { IState } from "../interfaces/AppInterfaces";
import { Colors, colorNode } from "../utils/Colors";

export function insertionSortIterate(
  state: IState,
  callback: (newState: IState) => void
) {
  let { data, list, sortedLimit, sortNode, compareNode, step } = {
    ...state,
  };

  if (sortedLimit && sortedLimit.next) {
    switch (step) {
      case 1:
        if (sortNode) colorNode(list.find(sortNode.data), Colors.sorted);
        if (compareNode) colorNode(list.find(compareNode.data), Colors.sorted);
        sortNode = sortedLimit.next;
        colorNode(list.find(sortNode.data), Colors.sorting);

        compareNode = sortNode;
        step = 2;
        break;
      case 2:
        // Loop through sorted sub-list until sortNode's value is higher than compareNode's
        const prevCompareNode = compareNode;
        if (compareNode) compareNode = compareNode.prev;
        if (
          !(compareNode && sortNode && sortNode.data.val < compareNode.data.val)
        ) {
          // If sortedNode's value is bigger than the sorted sub-list's max
          if (compareNode === sortedLimit) {
            sortedLimit = sortNode;
            step = 1;
          } else {
            step = 3;
          }
        }
        if (prevCompareNode && prevCompareNode !== sortNode)
          colorNode(list.find(prevCompareNode.data), Colors.sorted);
        if (compareNode) colorNode(list.find(compareNode.data), Colors.compare);
        break;
      case 3:
        let sortedNode: Node<Point> | null = null;
        // Move sortNode to its position in the sorted sub-list
        if (sortNode) {
          list.erase(sortNode);
          if (compareNode && compareNode.next)
            sortedNode = list.insert(compareNode.next, sortNode.data);
          else sortedNode = list.pushFront(sortNode.data);
        }
        data = list.traverse();
        if (sortedNode && sortedNode.next) {
          colorNode(list.find(sortedNode.next.data), Colors.sorted);
        }
        step = 1;
        break;
      default:
        console.error("Unknown step : " + step + ". Possibly dead-locked.");
        break;
    }
  } else {
    if (compareNode) colorNode(list.find(compareNode.data), Colors.sorted);
    if (sortNode) colorNode(list.find(sortNode.data), Colors.sorted);
    data = list.traverse();
    sortedLimit = null;
    compareNode = null;
    step = 0;
  }

  callback({ ...state, data, list, sortedLimit, sortNode, compareNode, step });
}
