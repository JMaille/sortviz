import { IState } from "../interfaces/AppInterfaces";
import { Colors, colorNode } from "../utils/Colors";

export function selectionSortIterate(
  state: IState,
  callback: (newState: IState) => void
) {
  let { data, list, sortedLimit, sortNode, compareNode, minNode, step } = {
    ...state,
  };

  if (sortedLimit && sortedLimit.next) {
    switch (step) {
      case 1:
        if (sortNode && sortNode !== sortedLimit)
          colorNode(list.find(sortNode.data), Colors.default);
        sortNode = sortedLimit.next;
        colorNode(list.find(sortNode.data), Colors.sorting);

        compareNode = sortNode;
        minNode = null;
        step = 2;
        break;
      case 2: // Find the min node in the unsorted sub-list
        let prevMin = minNode;
        let prevCompare = compareNode;

        // Assume sortNode is min
        if (!minNode) {
          minNode = sortNode;
        }
        if (compareNode) compareNode = compareNode.next;

        // If compareNode's value is less than minNode's, update minNode
        if (minNode && compareNode && compareNode.data.val < minNode.data.val) {
          minNode = compareNode;
        }

        if (compareNode) colorNode(list.find(compareNode.data), Colors.compare);
        if (prevCompare && prevCompare !== minNode)
          colorNode(list.find(prevCompare.data), Colors.default);
        if (minNode !== prevMin) {
          if (minNode) colorNode(list.find(minNode.data), Colors.min);
          if (prevMin) colorNode(list.find(prevMin.data), Colors.default);
        }
        if (!prevMin || prevMin === sortNode) {
          if (sortNode) colorNode(list.find(sortNode.data), Colors.sorting);
        }

        // All nodes have been checked, minNode is now the min of the unsorted sub-list
        if ((compareNode && !compareNode.next) || !compareNode) {
          if (minNode !== sortNode) {
            step = 3;
          } else {
            // sortNode is the min, no need to move it
            sortedLimit = sortNode;
            if (sortNode) colorNode(list.find(sortNode.data), Colors.sorted);
            if (compareNode)
              colorNode(list.find(compareNode.data), Colors.default);
            step = 1;
          }
        }
        break;
      case 3:
        if (compareNode && compareNode !== minNode)
          colorNode(list.find(compareNode.data), Colors.default);
        // Swap sortNode with minNode
        if (sortNode && minNode) {
          const newNodes = list.swap(sortNode, minNode);
          sortNode = newNodes.newNode1;
          minNode = newNodes.newNode2;
          colorNode(list.find(minNode.data), Colors.sorted);
        }
        data = list.traverse();

        sortedLimit = minNode;
        step = 1;
        break;
      default:
        console.error("Unknown step : " + step + ". Possibly dead-locked.");
        break;
    }
  } else {
    if (compareNode) colorNode(list.find(compareNode.data), Colors.sorted);
    if (sortNode) colorNode(list.find(sortNode.data), Colors.sorted);
    data = list.traverse();
    sortedLimit = null;
    compareNode = null;
    step = 0;
  }

  callback({
    ...state,
    data,
    list,
    sortedLimit,
    sortNode,
    compareNode,
    minNode,
    step,
  });
}
