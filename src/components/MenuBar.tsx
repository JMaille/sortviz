import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import Divider from "@material-ui/core/Divider";
import {
  DEFAULT_SIZE,
  DEFAULT_SPEED,
  DEFAULT_ALGORITHM,
} from "../utils/Defaults";
import IconButton from "@material-ui/core/IconButton";
import PauseIcon from "@material-ui/icons/Pause";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuBar: {
      backgroundColor: theme.palette.primary.main,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    spacedItem: {
      marginRight: theme.spacing(2),
    },
    slider: {
      maxHeight: 30,
      maxWidth: 100,
      color: theme.palette.secondary.dark,
      marginRight: theme.spacing(2),
    },
    pausePlay: {
      color: theme.palette.secondary.main,
    },
    select: {
      color: "white",
      borderColor: "white",
    },
  })
);

interface Props {
  paused: boolean;
  sorting: boolean;
  SortHandler: (event: React.MouseEvent<HTMLButtonElement>) => void;
  DataHandler: (event: React.MouseEvent<HTMLButtonElement>) => void;
  SpeedHandler: (
    event: React.ChangeEvent<{}>,
    value: number | number[]
  ) => void;
  SizeHandler: (event: React.ChangeEvent<{}>, value: number | number[]) => void;
  PauseHandler: (event: React.MouseEvent<HTMLButtonElement>) => void;
  AlgorithmHandler: (value: string) => void;
}

export default function MenuBar({
  paused,
  sorting,
  SortHandler,
  DataHandler,
  SpeedHandler,
  SizeHandler,
  PauseHandler,
  AlgorithmHandler,
}: Props) {
  const classes = useStyles();
  const [algorithm, setAlgorithm] = React.useState(DEFAULT_ALGORITHM);

  const handleChangeAlgorithm = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setAlgorithm(event.target.value as string);
    AlgorithmHandler(event.target.value as string);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.menuBar}>
          <Typography id="speed-slider" className={classes.spacedItem}>
            Size
          </Typography>
          <Slider
            className={classes.slider}
            defaultValue={DEFAULT_SIZE}
            aria-labelledby="speed-slider"
            min={10}
            step={1}
            max={100}
            onChange={SizeHandler}
            disabled={sorting}
          />
          <Button
            variant="contained"
            color="secondary"
            className={classes.menuButton}
            onClick={DataHandler}
            disabled={sorting}
          >
            New dataset
          </Button>

          <Divider
            orientation="vertical"
            flexItem
            className={classes.spacedItem}
          />
          <FormControl
            color="secondary"
            disabled={sorting}
            className={classes.spacedItem}
          >
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              value={algorithm}
              onChange={handleChangeAlgorithm}
              className={classes.select}
            >
              <MenuItem value={"insertion"}>Insertion sort</MenuItem>
              <MenuItem value={"selection"}>Selection sort</MenuItem>
              <MenuItem value={"bubble"}>Bubble sort</MenuItem>
            </Select>
          </FormControl>
          <Typography id="speed-slider" className={classes.spacedItem}>
            Speed
          </Typography>
          <Slider
            className={classes.slider}
            defaultValue={DEFAULT_SPEED}
            aria-labelledby="speed-slider"
            min={1}
            step={0.1}
            max={3}
            onChange={SpeedHandler}
          />
          <Button
            variant="contained"
            color="secondary"
            className={classes.menuButton}
            onClick={SortHandler}
            disabled={sorting}
          >
            Sort
          </Button>
          <IconButton
            aria-label="pause"
            onClick={PauseHandler}
            className={classes.pausePlay}
            disabled={!sorting}
          >
            {paused ? <PlayArrowIcon /> : <PauseIcon />}
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
}
