import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { Point } from "../utils/CustomTypes";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center",
    },
    bar: {
      marginRight: 1,
    },
  })
);

type Props = {
  data: Point[];
};

export default function SortVisualizer({ data }: Props) {
  const classes = useStyles();

  var bars: JSX.Element[] = [];
  const width = (): number => {
    let size = data.length;
    if (data[0].id === 0 || data[data.length - 1].id === 0) size -= 1;
    return window.innerWidth / 2 / size;
  };

  data.forEach((element) => {
    if (element.id !== 0) {
      bars.push(
        <div
          key={element.id}
          style={{
            height: element.val,
            width: width(),
            backgroundColor: element.color,
          }}
          className={classes.bar}
        />
      );
    }
  });

  return <div className={classes.root}>{bars}</div>;
}
