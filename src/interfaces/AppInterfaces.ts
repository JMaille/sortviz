import { Point } from "../utils/CustomTypes";
import { LinkedList, Node } from "../utils/LinkedList";

export interface IProps {}
export interface IState {
  data: Point[];
  list: LinkedList<Point>;
  sortedLimit: Node<Point> | null;
  sortNode: Node<Point> | null;
  compareNode: Node<Point> | null;
  minNode: Node<Point> | null;
  step: number;
  interval: number;
  size: number;
  paused: boolean;
  algorithm: string;
}
