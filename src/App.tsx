import React from "react";
import "./App.css";
import MenuBar from "./components/MenuBar";
import SortVisualizer from "./components/SortVisualizer";
import { Point } from "./utils/CustomTypes";
import { LinkedList } from "./utils/LinkedList";
import { randomIntFromInterval } from "./utils/MiscFunctions";
import { Colors } from "./utils/Colors";
import {
  DEFAULT_SIZE,
  DEFAULT_SPEED,
  DEFAULT_ALGORITHM,
  DEFAULT_POINT,
} from "./utils/Defaults";
import { IProps, IState } from "./interfaces/AppInterfaces";
import { insertionSortIterate } from "./algorithms/InsertionSort";
import { selectionSortIterate } from "./algorithms/SelectionSort";
import { bubbleSortIterate } from "./algorithms/BubbleSort";

export default class App extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      data: this.generateData(),
      list: new LinkedList<Point>(),
      sortedLimit: null,
      sortNode: null,
      compareNode: null,
      minNode: null,
      step: 0,
      interval: 10000 / 10 ** DEFAULT_SPEED,
      size: DEFAULT_SIZE,
      paused: false,
      algorithm: DEFAULT_ALGORITHM,
    };
  }

  private generateData() {
    let data: Point[] = [];
    const size = this.state ? this.state.size : DEFAULT_SIZE;
    for (let i = 1; i <= size; i++) {
      const val = randomIntFromInterval(30, 300);
      data.push({ id: i, val: val, color: Colors.default });
    }
    return data;
  }

  public newData() {
    let data = this.generateData();
    this.setState({ data });
  }

  public startSorting() {
    const data = this.state.data;
    const list = new LinkedList<Point>();
    data.forEach((point) => {
      list.pushBack(point);
    });

    const algorithm = this.state.algorithm;
    switch (algorithm) {
      case "insertion":
        this.setState({ list, sortedLimit: list.begin(), step: 1 }, () => {
          this.continueSorting(insertionSortIterate);
        });
        break;
      case "selection":
        list.pushFront(DEFAULT_POINT);
        this.setState(
          { list, sortedLimit: list.begin(), sortNode: list.begin(), step: 1 },
          () => {
            this.continueSorting(selectionSortIterate);
          }
        );
        break;
      case "bubble":
        list.pushBack(DEFAULT_POINT);
        this.setState(
          { list, sortedLimit: list.end(), sortNode: list.begin(), step: 1 },
          () => {
            this.continueSorting(bubbleSortIterate);
          }
        );
        break;

      default:
        break;
    }
  }

  private continueSorting(
    iterate: (state: IState, callback: (newState: IState) => void) => void
  ) {
    const state = this.state;
    if (state.sortedLimit && !state.paused) {
      setTimeout(() => {
        iterate(
          state,
          ({
            data,
            list,
            sortedLimit,
            sortNode,
            compareNode,
            minNode,
            step,
          }) => {
            this.setState(
              { data, list, sortedLimit, sortNode, compareNode, minNode, step },
              () => this.continueSorting(iterate)
            );
          }
        );
      }, this.state.interval);
    }
  }

  public handlePause() {
    this.setState(
      (prevState) => {
        return { paused: !prevState.paused };
      },
      () => {
        if (!this.state.paused) {
          const algorithm = this.state.algorithm;
          switch (algorithm) {
            case "insertion":
              this.continueSorting(insertionSortIterate);
              break;
            case "selection":
              this.continueSorting(selectionSortIterate);
              break;
            case "bubble":
              this.continueSorting(bubbleSortIterate);
              break;

            default:
              break;
          }
        }
      }
    );
  }

  public updateSpeed(value: number | number[]) {
    if (!Array.isArray(value)) {
      this.setState({ interval: 10000 / 10 ** value });
    }
  }

  public updateSize(value: number | number[]) {
    if (!Array.isArray(value) && this.state.size !== value) {
      this.setState({ size: value }, () => this.newData());
    }
  }

  public updateAlgorithm(value: string) {
    this.setState({ algorithm: value });
  }

  render() {
    return (
      <div className="App">
        <MenuBar
          paused={this.state.paused}
          sorting={this.state.sortedLimit != null}
          SortHandler={() => this.startSorting()}
          DataHandler={() => this.newData()}
          SpeedHandler={(_, value) => this.updateSpeed(value)}
          SizeHandler={(_, value) => this.updateSize(value)}
          PauseHandler={() => this.handlePause()}
          AlgorithmHandler={(value) => this.updateAlgorithm(value)}
        />
        <SortVisualizer data={this.state.data} />
      </div>
    );
  }
}
