import { Point } from "./CustomTypes";
import { Node } from "./LinkedList";

export const Colors = {
  default: "#1000ff",
  sorted: "#9900ff",
  sorting: "#ffd600",
  compare: "#43a047",
  min: "#ff1744",
};

export function colorNode(node: Node<Point> | null, color: string) {
  if (node) node.data.color = color;
}