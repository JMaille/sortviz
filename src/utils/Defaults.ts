import { Point } from "../utils/CustomTypes";
import { Colors } from "../utils/Colors";

export const DEFAULT_SIZE = 20;
export const DEFAULT_SPEED = 1.5;
export const DEFAULT_ALGORITHM = "insertion";

export const DEFAULT_POINT: Point = {
  id: 0,
  val: 0,
  color: Colors.default,
};