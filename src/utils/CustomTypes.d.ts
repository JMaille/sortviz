import { LinkedList, Node } from "./LinkedList";

export type Point = {
  id: number;
  val: number;
  color?: string;
};

export type SortingData = {
  data: Point[];
  list: LinkedList<Point>;
  sortedLimit: Node<Point> | null;
};
