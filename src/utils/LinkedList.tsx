export class Node<T> {
  public next: Node<T> | null = null;
  public prev: Node<T> | null = null;
  constructor(public data: T) {}
}

interface ILinkedList<T> {
  begin(): Node<T> | null;
  end(): Node<T> | null;
  pushFront(data: T): Node<T>;
  pushBack(data: T): Node<T>;
  insert(position: Node<T>, data: T): Node<T>;
  popFront(): void;
  popBack(): void;
  erase(node: Node<T>): void;
  traverse(): T[];
  size(): number;
  find(data: T): Node<T> | null;
  swap(
    node1: Node<T>,
    node2: Node<T>
  ): { newNode1: Node<T>; newNode2: Node<T> };
}

export class LinkedList<T> implements ILinkedList<T> {
  private head: Node<T> | null = null;
  private tail: Node<T> | null = null;
  private length: number;

  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }

  public begin(): Node<T> | null {
    return this.head;
  }

  public end(): Node<T> | null {
    return this.tail;
  }

  public pushFront(data: T): Node<T> {
    const newNode = new Node(data);
    if (!this.head) {
      // List is empty
      this.head = newNode;
      this.tail = newNode;
    } else {
      this.head.prev = newNode;
      newNode.prev = null;
      newNode.next = this.head;
      this.head = newNode;
    }
    this.length++;
    return newNode;
  }

  public pushBack(data: T): Node<T> {
    const newNode = new Node(data);
    if (!this.tail) {
      // List is empty
      this.head = newNode;
      this.tail = newNode;
    } else {
      this.tail.next = newNode;
      newNode.prev = this.tail;
      newNode.next = null;
      this.tail = newNode;
    }
    this.length++;
    return newNode;
  }

  // New node is inserted before the position node, the list cannot be empty
  public insert(position: Node<T>, data: T): Node<T> {
    const newNode = new Node(data);
    if (!position.prev) {
      this.head = newNode;
    } else {
      position.prev.next = newNode;
    }
    newNode.prev = position.prev;
    newNode.next = position;
    position.prev = newNode;
    this.length++;
    return newNode;
  }

  public popFront(): void {
    if (this.head) {
      if (!this.head.next) {
        // Only 1 node in list
        this.head = null;
        this.tail = null;
      } else {
        this.head.next.prev = null;
        this.head = this.head.next;
      }
      this.length--;
    }
  }

  public popBack(): void {
    if (this.tail) {
      if (!this.tail.prev) {
        // Only 1 node in list
        this.head = null;
        this.tail = null;
      } else {
        this.tail.prev.next = null;
        this.tail = this.tail.prev;
      }
      this.length--;
    }
  }

  public erase(node: Node<T>): void {
    if (!node.prev) {
      this.head = node.next;
      if (this.head) this.head.prev = null;
    } else if (!node.next) {
      this.tail = node.prev;
      if (this.tail) this.tail.next = null;
    } else {
      node.prev.next = node.next;
      node.next.prev = node.prev;
    }
    this.length--;
  }

  public traverse(): T[] {
    const array: T[] = [];
    if (!this.head) {
      return array;
    }

    const addToArray = (node: Node<T>): T[] => {
      array.push(node.data);
      return node.next ? addToArray(node.next) : array;
    };
    return addToArray(this.head);
  }

  public size(): number {
    return this.length;
  }

  public find(data: T): Node<T> | null {
    let node = this.head;
    while (node) {
      if (node.data === data) break;
      else node = node.next;
    }
    return node;
  }

  public swap(
    node1: Node<T>,
    node2: Node<T>
  ): { newNode1: Node<T>; newNode2: Node<T> } {
    if (node1 === node2) return { newNode1: node1, newNode2: node2 };

    let newNode1 = node1;
    let newNode2 = node2;

    if (node2.next !== node1) {
      this.erase(node1);
      if (node2.next) newNode1 = this.insert(node2.next, node1.data);
      else newNode1 = this.pushBack(node1.data);
    }

    if (node1.next !== node2) {
      this.erase(node2);
      if (node1.next) newNode2 = this.insert(node1.next, node2.data);
      else newNode2 = this.pushBack(node2.data);
    }

    return { newNode1, newNode2 };
  }
}
