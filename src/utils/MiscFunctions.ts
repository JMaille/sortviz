export function randomIntFromInterval(min: number, max: number) {
  return Math.floor(
    Math.random() * (Math.floor(max) - Math.floor(min) + 1) + Math.floor(min)
  );
}
